#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

PGPASSWORD=$(kubectl get secret keycloak.sso-postgres.credentials.postgresql.acid.zalan.do -o 'jsonpath={.data.password}' -n keycloak| base64 -d)
KEYCLOAK_ADMIN_PWD=$(openssl rand -base64 48 | cut -c1-16)
WILDFLY_ADMIN_PWD=$(openssl rand -base64 48 | cut -c1-16)

cat << EOF > sso-db-unsealed-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: keycloak-db-secret
  namespace: keycloak
stringData:
  KEYCLOAK_DATABASE_NAME: "root"
  KEYCLOAK_DATABASE_PORT: "5432"
  KEYCLOAK_DATABASE_HOST: "sso-postgres"
  KEYCLOAK_DATABASE_PASSWORD: $PGPASSWORD
  KEYCLOAK_DATABASE_USER: "keycloak"
EOF

cat << EOF > sso-admin-secrets.yaml
apiVersion: v1
kind: Secret
metadata:
  name: sso-admin-secrets
  namespace: keycloak
stringData:
  adminPassword: $KEYCLOAK_ADMIN_PWD
  managementPassword: $WILDFLY_ADMIN_PWD
EOF

kubeseal --format=yaml < sso-db-unsealed-secret.yaml > sealed-sso-db-secret.yaml
kubeseal --format=yaml < sso-admin-secrets.yaml > sealed-sso-admin-secrets.yaml

cat << EOF > Chart.yaml
apiVersion: v2
name: keycloak
description: A Helm chart for Kubernetes
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.0.1

dependencies:
- name: keycloak
  version: 3.0.4
  repository: https://charts.bitnami.com/bitnami
EOF

cat << EOF > values.yaml
keycloak:
  proxyAddressForwarding: true
  auth:
    adminUser: "admin"
    managementUser: "admin"
    existingSecret:
      name: sso-admin-secrets
      keyMapping:
        admin-password: adminPassword
        management-password: managementPassword
  serviceDiscovery:
    enabled: true
  cache:
    ownersCount: 2
    authOwnersCount: 2
  extraEnvVars:
    - name: JAVA_OPTS_APPEND
      value: >-
        -XX:+UseContainerSupport
        -XX:MaxRAMPercentage=50.0
  replicaCount: 2
  service:
    type: ClusterIP
  rbac:
    create: true
  postgresql:
    enabled: false
  externalDatabase:
    existingSecret: keycloak-db-secret
EOF

mkdir auth/sso/templates
mv sealed-sso-db-secret.yaml auth/sso/templates
mv sealed-sso-admin-secrets.yaml auth/sso/templates
mv auth/sso/keycloak-postgres.yaml auth/sso/templates/keycloak-postgres.yaml
mv Chart.yaml auth/sso
mv values.yaml auth/sso

rm sso-db-unsealed-secret.yaml
rm sso-admin-secrets.yaml

git add auth
git commit -m "add db secret and keycloak custom resource"
git push origin main

argocd app sync sso

echo "This will take a little while (maybe 7-8 minutes to get both pods running)"
echo ""
echo "You can use the following commands for the initial keycloak passwords:"
echo ""
echo "Keycloak Admin User (admin):"
echo 'kubectl -n keycloak get secret sso-admin-secrets -o jsonpath="{.data.adminPassword}" | base64 -d'
echo ""
echo ""
echo "Java Wildfly Management User (admin):"
echo 'kubectl -n keycloak get secret sso-admin-secrets -o jsonpath="{.data.managementPassword}" | base64 -d'
