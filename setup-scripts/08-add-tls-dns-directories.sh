#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${DIGITALOCEAN_PAT_DNS}failcheck"
failcheck="${DOMAIN_NAME}failcheck"

cat << EOF > cert-manager.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cert-manager
  namespace: argocd
spec:
  project: default
  source:
    chart: cert-manager
    repoURL: https://charts.jetstack.io
    targetRevision: 1.3.1
    helm:
      parameters:
      - name: installCRDs
        value: "true"
  destination:
    namespace: cert-manager
    server: https://kubernetes.default.svc
  syncPolicy:
    syncOptions:
    - Validate=false
    automated:
      selfHeal: true  
  ignoreDifferences:
  - group: apiextensions.k8s.io
    kind: CustomResourceDefinition
    jsonPointers:
    - /status
---
apiVersion: v1
kind: Namespace
metadata:
  name: cert-manager
EOF

echo -n $DIGITALOCEAN_PAT_DNS | kubectl create secret generic dnspat --dry-run=client --from-file=pat=/dev/stdin -o json > do_pat_default.json
kubeseal --format=yaml < do_pat_default.json > sealed-pat-default.yaml

cat << EOF > external-dns.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: external-dns
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: k8s-core/external-dns
  destination:
    server: https://kubernetes.default.svc
    namespace: default
  syncPolicy:
    automated:
      selfHeal: true  
EOF

cat << EOF > externaldns-resources.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: external-dns
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: external-dns
rules:
- apiGroups: [""]
  resources: ["services","endpoints","pods"]
  verbs: ["get","watch","list"]
- apiGroups: ["extensions","networking.k8s.io"]
  resources: ["ingresses"] 
  verbs: ["get","watch","list"]
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["get", "watch", "list"]
- apiGroups: [""]
  resources: ["endpoints"]
  verbs: ["get", "watch", "list"]  
- apiGroups: ["networking.istio.io"]
  resources: ["gateways", "virtualservices"]
  verbs: ["get","watch","list"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: external-dns-viewer
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: external-dns
subjects:
- kind: ServiceAccount
  name: external-dns
  namespace: default
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: external-dns
spec:
  replicas: 1
  selector:
    matchLabels:
      app: external-dns
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: external-dns
    spec:
      serviceAccountName: external-dns
      containers:
      - name: external-dns
        image: k8s.gcr.io/external-dns/external-dns:v0.7.6
        args:
        - --source=service # ingress is also possible
        - --source=ingress
        - --domain-filter=$DOMAIN_NAME # (optional) limit to only example.com domains; change to match the zone created above.
        - --provider=digitalocean
        env:
        - name: DO_TOKEN
          valueFrom:
            secretKeyRef:
              name: dnspat
              key: pat
EOF

mv cert-manager.yaml k8s-core
mv external-dns.yaml k8s-core

rm do_pat_default.json

mkdir k8s-core/external-dns
mv externaldns-resources.yaml k8s-core/external-dns
mv sealed-pat-default.yaml k8s-core/external-dns

git add k8s-core
git commit -m "add argoCD applications for core k8s packages"
git push origin main

argocd app sync k8s-core-apps
