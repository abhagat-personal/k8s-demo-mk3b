#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

kubeseal --fetch-cert > sealed-secrets-public-key.pem

git add sealed-secrets-public-key.pem

echo "Your developers can now add secrets using:"
echo "kubeseal --cert sealed-secrets-public-key.pem"

git commit -m "add sealed secrets public key"
git push origin main
