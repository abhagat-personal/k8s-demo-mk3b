#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"

cat << EOF > falco.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: falco
  annotations:
    linkerd.io/inject: enabled
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: falco
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: k8s-core/falco
  destination:
    server: https://kubernetes.default.svc
    namespace: falco
  syncPolicy:
    automated:
      selfHeal: true  
EOF

cat << EOF > Chart.yaml
apiVersion: v2
name: falco
description: A Helm chart for Kubernetes
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.0.1

dependencies:
- name: falco
  version: 1.11.1
  repository: https://falcosecurity.github.io/charts
EOF

cat << EOF > values.yaml
EOF

mkdir k8s-core/falco
mv falco.yaml k8s-core
mv Chart.yaml k8s-core/falco
mv values.yaml k8s-core/falco

git add k8s-core
git commit -m "add falco"
git push origin main
