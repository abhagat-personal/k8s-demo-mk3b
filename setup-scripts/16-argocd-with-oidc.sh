#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

argocdOidcSecret=$1

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${GITLAB_PAT}failcheck"
failcheck="${KEYCLOAK_REALM}failcheck"
failcheck="${argocdOidcSecret}failcheck"

# was not able to find a way to do this declaratively
kubectl patch secret argocd-secret -n argocd -p='{"stringData":{"oidc.keycloak.clientSecret": '"\"$argocdOidcSecret\""'}}' -v=1

base64_pat=$(echo -n $GITLAB_PAT | base64) 
base64_username=$(echo -n "username" | base64)

cat << EOF > argocd-root-repo-secret.yaml
apiVersion: v1
kind: Secret
metadata:
  creationTimestamp: null
  name: argocd-root-repo-secret
  namespace: argocd
data:
  username: $base64_username
  password: $base64_pat
EOF

kubeseal --format=yaml < argocd-root-repo-secret.yaml> sealed-argocd-root-repo-secret.yaml
rm argocd-root-repo-secret.yaml

oidcSecretPath='$oidc.keycloak.clientSecret'

cat << EOF > values.yaml
argo-cd:
  installCRDs: false
  server:
    extraArgs:
      - "--insecure"
    config:
      url: https://argo.devops.$DOMAIN_NAME
      repositories: |-
        - name: root-repository
          passwordSecret:
            key: password
            name: argocd-root-repo-secret
          url: $GITLAB_REPO_URL
          usernameSecret:
            key: username
            name: argocd-root-repo-secret
      oidc.config: |
        name: $KEYCLOAK_REALM
        issuer: https://auth.$DOMAIN_NAME/auth/realms/$KEYCLOAK_REALM
        clientID: argocd
        clientSecret: $oidcSecretPath
        requestedScopes: ["openid", "profile", "email", "groups"] 
    rbacConfig:
      policy.csv: |
        g, ArgoCDAdmin, role:admin
        g, ArgoCDReadOnly, role:readonly
        g, /ArgoCDAdmin, role:admin
        g, /ArgoCDReadOnly, role:readonly
EOF

cat << EOF > http-argocd-ingress.yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: argocd-server-http-ingress
  namespace: argocd
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTP"
spec:
  rules:
  - http:
      paths:
      - backend:
          serviceName: argocd-server
          servicePort: http
    host: argo.devops.$DOMAIN_NAME
  tls:
  - hosts:
    - argo.devops.$DOMAIN_NAME
    secretName: argocd-secret # do not change, this is provided by Argo CD
EOF

mkdir argocd/templates
mv values.yaml argocd/values.yaml
mv http-argocd-ingress.yaml argocd/templates
mv sealed-argocd-root-repo-secret.yaml argocd/templates

git add argocd
git commit -m "set up argoCD OIDC user based access"
git push origin main

echo "At this point, you're going to want to throughly test that your keycloak users"
echo "have access to ArgoCD, with the right permissions depending their groups."
echo "In the next file, we'll delete the initial admin secret (which can be regenerated easily)."
echo "You'll no longer need to port forward to the cluster either."
echo "See https://argoproj.github.io/argo-cd/operator-manual/user-management/keycloak/ for some useful"
echo "instructions on setting up ArgoCD access via keycloak."
